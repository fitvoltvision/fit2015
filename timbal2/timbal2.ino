#include <FastLED.h>


#define LED_PIN     11
#define MIC_PIN     A0
#define BUT_PIN     15

#define NUM_LEDS    65
#define BRIGHTNESS  64
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB

uint8_t COLOR       = 0;
CRGB leds[NUM_LEDS];

int barrier = 700;
int mode;
int cont = 0;
void setup() {
   delay( 3000 ); // power-up safety delay
   // setup the LED strip
   FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
   FastLED.setBrightness(  BRIGHTNESS );
   for(int i = 0; i < NUM_LEDS/2; i++) {
     leds[i] = CHSV(COLOR, 255, 255);
   }
   for(int i = NUM_LEDS/2; i < NUM_LEDS; i++) {
     leds[i] = CHSV((COLOR + 128)%255, 255, 255);
   }
   FastLED.show();
   Serial.begin(9600);
   pinMode(BUT_PIN, INPUT_PULLUP);
   //attachInterrupt(BUT_PIN, changeMode, CHANGE);
   mode = 1;
}


void loop(){
  int mic = analogRead(MIC_PIN);
  //Serial.print("  Microphone: ");
  //Serial.println(mic);

  if(mic > barrier) {
    showLEDS_hit();
  } else {
    showLEDS_normal();
  }
  if(digitalRead(BUT_PIN) == LOW) {
    changeMode();
  }
  //if(cont == 500) {
    //changeMode();
    //cont = 0;
  //}
//  cont++;
  //Serial.println(digitalRead(BUT_PIN));
  

  delay(10);
}

void showLEDS_normal(){
  switch (mode) {
    case 1: case 2: case 3: case 4: case 5:
      FastLED.showColor(CHSV(COLOR, 255, 255));
      break;
    case 6:
      FastLED.showColor(CHSV(COLOR++, 255, 255));
      break;
    case  7:
      for(int i = 0; i < NUM_LEDS/2; i++) {
        leds[i] = CHSV(COLOR, 255, 255);
      }
      for(int i = NUM_LEDS/2; i < NUM_LEDS; i++) {
        leds[i] = CHSV((COLOR + 128)%255, 255, 255);
      }
      FastLED.show();  
      break;
  }

}

void showLEDS_hit() {
  uint8_t color_temp = COLOR;
  COLOR = (COLOR + 128)%255;
  switch (mode) {
    case 1: case 2: case 3: case 4: case 5: case 6:
      FastLED.showColor(CHSV(COLOR, 255, 255));
      break;
    case 7:
      for(int i = 0; i < NUM_LEDS/2; i++) {
        leds[i] = CHSV(COLOR, 255, 255);
      }
      for(int i = NUM_LEDS/2; i < NUM_LEDS; i++) {
        leds[i] = CHSV((COLOR + 128)%255, 255, 255);
      }
      FastLED.show();
      break;
  }
  COLOR = color_temp;
  delay(50);
}

void changeMode() {
  mode++;
  mode = (mode == 8)?1:mode;
 
  Serial.println(mode);
  switch (mode) {
    case 1:
      COLOR = 0; // RED
      break;
    case 2:
      COLOR = 96; // GREEN
      break;
    case 3:
      COLOR = 160; // BLUE
      break;
    case 4:
      COLOR = 64; // YELLOW
      break;
    case 5:
      COLOR = 192; // PURPLE
      break;
    case 6: break; // rainbow
    case 7:
      COLOR = 100;
      break;
  }
  delay(2000);
}
